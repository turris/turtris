#!/bin/sh

ROUTERS="1 2 3 4 5 6 7 8 9 10"

I=0

for R in $ROUTERS; do
	echo "=== Runnining on 192.168.1.$R ==="

	ssh -i routers/turtris_key root@192.168.1.$R -- "$@"

	#scp -r -i routers/turtris_key init.d/turtetris-slave root@192.168.1.$R:/etc/init.d/
	#ssh -i routers/turtris_key root@192.168.1.$R -- /etc/init.d/turtetris-slave enable

	#scp -r -i routers/turtris_key turtetris_slave root@192.168.1.$R:/usr/lib/python3.9/site-packages/
	#ssh -i routers/turtris_key root@192.168.1.$R -- /etc/init.d/turtetris-slave restart

	#ssh-copy-id -i routers/turtris_key root@192.168.1.$R
	I=$(expr $I + 1)
done
