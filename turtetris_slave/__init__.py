import socket
from . import leds
from time import time

def socket_init():
    "Initialize socket"
    UDP_IP = "0.0.0.0"
    UDP_PORT = 2255

    # create UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((UDP_IP, UDP_PORT))

    return sock


def main():
    "Main function"
    print("Starting turtris slave")
    sock = socket_init()
    leds.prepare()
    cache = bytearray(bytes.fromhex('000000') * 12)
    while True:
        data, addr = sock.recvfrom(36)
        leds.output(data, cache)



if __name__ == '__main__':
    main()
