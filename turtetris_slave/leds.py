import subprocess


def prepare():
    "Prepare leds"
    for i in range(len(__MAP__)):
        cmd = f"echo 'none' >/sys/class/leds/rgb:{__MAP__[i]}/trigger"
        subprocess.run(cmd, shell=True, check=True)

        cmd = f"echo '0 0 0' >/sys/class/leds/rgb:{__MAP__[i]}/multi_intensity"
        subprocess.run(cmd, shell=True, check=True)

        cmd = f"echo '1' >/sys/class/leds/rgb:{__MAP__[i]}/brightness"
        subprocess.run(cmd, shell=True, check=True)


def clear():
    "Clear previous changes"
    call("rainbow all auto", shell=True)


__MAP__ = [
    'power',
    'lan-0',
    'lan-1',
    'lan-2',
    'lan-3',
    'lan-4',
    'wan',
    'wlan-1',
    'wlan-2',
    'wlan-3',
    'indicator-1',
    'indicator-2'
]


def output(data, cache):
    "Output received data to leds"
    for i in range(len(__MAP__)):
        idx = i*3
        print(f"{data[idx+1]} and {cache[idx+1]}")
        if data[idx] != cache[idx] or data[idx+1] != cache[idx+1]\
                                   or data[idx+2] != cache[idx+2]:
            cache[idx] = data[idx]
            cache[idx+1] = data[idx+1]
            cache[idx+2] = data[idx+2]
            # create string in format 255 255 255 from data
            s = f"{str(data[idx])} {str(data[idx+1])} {str(data[idx+2])}"

            # set led with the previously created string
            cmd = f"echo '{s}' >/sys/class/leds/rgb:{__MAP__[i]}/multi_intensity"
            subprocess.run(cmd, shell=True, check=True)